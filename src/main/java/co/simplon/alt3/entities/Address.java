package co.simplon.alt3.entities;

import co.simplon.alt3.validators.zipcodevalidators.BelgianFormatValidator;
import co.simplon.alt3.validators.zipcodevalidators.FrenchFormatValidator;
import co.simplon.alt3.validators.zipcodevalidators.IZipCodeValidator;
import co.simplon.alt3.validators.zipcodevalidators.ZipCodeValidatorFactory;

/**
 * Entité Address - permet de stocker l'adresse dans un pays francophone
 * 
 * @author Barth DELUY <barth@barthdeluy.me>
 * @version 1.1
 */
public class Address {
    /**
     * Id auto-généré dans la base
     */
    private int id;
    /**
     * Numéro dans la rue - String pour gérer le "2bis"
     */
    private String number;
    private String street;
    private String zipCode;
    private String city;
    private String country;
    private IZipCodeValidator validator = new FrenchFormatValidator();

    /**
     * Instancie la classe - Vérifie le format du Code Postal avant instanciation
     * 
     * @param number numéro dans la rue
     * @param street rue
     * @param zipCode code postal
     * @param city ville
     * @param country pays
     * @throws Exception Le code postal n'est pas valide pour le pays sélectionné
     */
    public Address(String number, String street, String zipCode, String city, String country) throws Exception {
        this.number = number;
        this.street = street;
        this.city = city;
        this.setCountry(country);
        this.setZipCode(zipCode);
    }
    /**
     * 
     */
    public Address() {
    }
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }
    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }
    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }
    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }
    /**
     * @return the zipCode
     */
    public String getZipCode() {
        return zipCode;
    }
    /**
     * @param zipCode the zipCode to set
     */
    public void setZipCode(String zipCode) throws Exception {
        if(this.checkZipCode(zipCode)) {
            this.zipCode = zipCode;
        }
    }
    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }
    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }
    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }
    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
        this.validator = ZipCodeValidatorFactory.getValidator(country);
    }

    public void setValidator(IZipCodeValidator zcv) {
        this.validator = zcv;
    }

    private boolean checkZipCode(String zipCode) throws Exception {
        return this.validator.check(zipCode);
    }

}
