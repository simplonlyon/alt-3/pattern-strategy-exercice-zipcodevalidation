package co.simplon.alt3.validators.zipcodevalidators;

public interface IZipCodeValidator {
    public boolean check(String zipcode) throws Exception;
}
