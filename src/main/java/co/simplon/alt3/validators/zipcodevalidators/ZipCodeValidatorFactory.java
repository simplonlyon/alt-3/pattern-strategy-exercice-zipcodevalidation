package co.simplon.alt3.validators.zipcodevalidators;

public class ZipCodeValidatorFactory {
    public static IZipCodeValidator getValidator(String country) {
        switch(country) {
            case "Andorre" : 
                return new AndorranFormatValidator();
            case "Belgique" :
            case "Luxembourg" :
                return new BelgianFormatValidator();
            default:
                return new FrenchFormatValidator();
        }
    }
}
