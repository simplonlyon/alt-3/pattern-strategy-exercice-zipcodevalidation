package co.simplon.alt3.validators.zipcodevalidators;

public class FrenchFormatValidator implements IZipCodeValidator {
    public boolean check(String zipcode) throws Exception {
        if(!zipcode.matches("^[0-9]{5}$")) {
            throw new Exception("Le code postal doit être sur 5 chiffres");
        }
        return true;
    }
}
