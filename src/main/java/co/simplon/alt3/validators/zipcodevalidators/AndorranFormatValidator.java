package co.simplon.alt3.validators.zipcodevalidators;

public class AndorranFormatValidator implements IZipCodeValidator {
    public boolean check(String zipcode) throws Exception {
        if(!zipcode.matches("^AD[0-9]{3}$")) {
            throw new Exception("Le code postal commencer par AD suivi par 3 chiffres");
        }
        return true;
    }
    
}
