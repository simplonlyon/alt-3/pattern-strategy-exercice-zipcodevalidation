package co.simplon.alt3.validators.zipcodevalidators;

public class BelgianFormatValidator implements IZipCodeValidator {
    public boolean check(String zipcode) throws Exception {
        if(!zipcode.matches("^[0-9]{4}$")) {
            throw new Exception("Le code postal doit être sur 4 chiffres");
        }
        return true;
    }
}
